interface HTMLVideoElement {
  webkitSetPresentationMode(mode: "inline" | "picture-in-picture"): void
}

interface CSSStyleDeclaration {
  webkitBackdropFilter: string
}

(() => {
  const slice = <T>(arr: ArrayLike<T>): T[] => Array.prototype.slice.call(arr, 0)
  const canSelect = (node: any): node is Document => node && node.querySelectorAll
  const color = "rgba(50, 100, 255, 0.3)"

  const findFrames = (doc?: Document): Document[] => {
    if (!canSelect(doc)) return []

    return (
      slice(doc.querySelectorAll("iframe"))
      .map(iframe => findFrames(iframe.contentDocument))
      .reduce((acc, arr) => acc.concat(arr), [])
      .concat([doc])
    )
  }

  const map: WeakMap<HTMLDivElement, HTMLVideoElement> = new WeakMap()

  const overlay =
    findFrames(document)
    .filter(canSelect)
    .map(doc => doc.querySelectorAll("video"))
    .reduce((acc, arr) => acc.concat(slice(arr)), <HTMLVideoElement[]>[])
    .map(video => {
      const {scrollX, scrollY} = window
      const {top, left, width, height} = video.getBoundingClientRect()

      const el = document.createElement("div")
      el.style.background = color
      el.style.boxShadow = `black 3px 3px 50px, ${color} 0 0 50px inset`
      el.style.borderRadius = "3px"
      el.style.position = "absolute"
      el.style.width = `${width}px`
      el.style.height = `${height}px`
      el.style.top = `${scrollY + top}px`
      el.style.left = `${scrollX + left}px`
      el.style.cursor = "pointer"

      map.set(el, video)

      return el
    })
    .reduce((parent, div) => {
      parent.appendChild(div)
      return parent
    }, (() => {
      const overlayDiv = document.createElement("div")
      overlayDiv.style.background = "rgba(255, 255, 255, 0.5)"
      overlayDiv.style.webkitBackdropFilter = "blur(8px) grayscale(100%)"
      overlayDiv.style.width = "100vw"
      overlayDiv.style.height = "100vh"
      overlayDiv.style.position = "fixed"

      const el = document.createElement("div")
      el.style.zIndex = "9999999999"
      el.style.position = "absolute"
      el.style.cursor = "default"
      el.appendChild(overlayDiv)
      return el
    })())
  
  const listener = evt => {
    const video: HTMLVideoElement = map.get(<HTMLDivElement>evt.target)
    if (video) video.webkitSetPresentationMode("picture-in-picture")
    overlay.removeEventListener("click", listener)
    document.body.removeChild(overlay)
    return false
  }
  
  overlay.addEventListener("click", listener)

  document.body.insertBefore(overlay, document.body.firstChild)

})()
