# PiPify
Little bookmarklet to choose a video to PiPify from a webpage. Needs macOS Sierra and Safari.

Visit [https://cprecioso.gitlab.io/pipify/](https://cprecioso.gitlab.io/pipify/) to install.
