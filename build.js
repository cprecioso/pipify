const {readFileSync, writeFileSync, mkdirSync} = require("fs")
const {resolve} = require("path")
const {compileFile} = require("pug")

const encoding = "utf8"
const pretty = true

const linkHref = readFileSync(resolve(__dirname, "dist", "index.min.js"), {encoding})
const template = compileFile(resolve(__dirname, "public", "index.pug"), {pretty})
const html = template({linkHref})
try { mkdirSync(resolve(__dirname, "public")) } catch (_) {}
writeFileSync(resolve(__dirname, "public", "index.html"), html, {encoding})
